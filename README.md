# RSSI Scout

An important metric in wireless networking is RSSI (Received Signal Strength Indicator). RSSI Scout,
is an Android software application that retrieves the RSSI for each wireless network in the
environment and reports this reading to an imaginary API.

# Design Overview
RSSI Scout sends a post request every 20 seconds via a REST API call.
Due long running task natue of the application a Android service class is implemented that uses Broadcast Recievers
to manage the scouting of RSSI data and the submission of this data on a asyncrounoys background thread that:
1. Does not Strain the UI thread
2. Runs in the background while the app is not open, while at the same time not draining the battery fast.

# Mechanism to demonstrate the requests being made
Every 20 seconds the application scans for new RSSI data, this data is then caputured in a JSON payload and sent to the 
mock endpoint using the Android Retrofit Async lib to make a POST call.
Because this is a mock endpoint the call will enter the onFailure() function on line 118 of the RssiSyncService.
To show the JSON payload built by the scan I save the new payload to device storage that can be located under internal storage/Android/data/com.stuartjacobs.rssiscout/files

# Automated Testing
To test the Wifi manager class in android required a host of thirsd party Mocking libs beyond the normal Mockito and Junit libraries.
For the sake of this assesment running on multiplte devices and not wanting to run into dependancy issues I exclueded the test from the source file.
Here is an example of how I would have mocked the WiFi Manager to test my scanning function:
https://android.googlesource.com/platform/frameworks/base/+/master/wifi/tests/src/android/net/wifi/WifiManagerTest.java