package com.stuartjacobs.rssiscout.rs.rssi;

import com.stuartjacobs.rssiscout.Environment;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface PostRssi {
    @Multipart
    @POST("rssi")
    @Headers({
            "X-Api-Key:" + Environment.API_KEY,
    })
    Call<ResponseBody> postRssiData(@Part MultipartBody.Part rssi_json);
}
