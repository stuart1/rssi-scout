package com.stuartjacobs.rssiscout.util;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;
import androidx.annotation.Nullable;
import com.stuartjacobs.rssiscout.Environment;
import com.stuartjacobs.rssiscout.rs.rssi.PostRssi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RssiSyncService extends Service {

    private Handler handler;

    public static final long DEFAULT_SYNC_INTERVAL = 20 * 1000;
    private WifiManager wifiManager;
    private List<ScanResult> scanResults;

    private Runnable runnableService = new Runnable() {
        @Override
        public void run() {
            //Scan Networks
            wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

            //Sync RSSI data every 20 seconds
            Toast.makeText(getApplicationContext(),
                    "Posting RSSI Data...",
                    Toast.LENGTH_SHORT).show();

            registerReceiver(rssiReciever, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            wifiManager.startScan();


            handler.postDelayed(runnableService, DEFAULT_SYNC_INTERVAL);
        }
    };

    BroadcastReceiver rssiReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            scanResults = wifiManager.getScanResults();
            unregisterReceiver(this);
            JSONArray rssiData = new JSONArray();
            for (ScanResult scanResult : scanResults) {
                JSONObject jsonRssiNetwork = new JSONObject();

                try {
                    jsonRssiNetwork.put("dtDetected", Calendar.getInstance().getTime());
                    jsonRssiNetwork.put("networkName", scanResult.SSID);
                    jsonRssiNetwork.put("dBmLevel", scanResult.level);
                    jsonRssiNetwork.put("signalStrenght", WifiManager.calculateSignalLevel(scanResult.level, 5));
                    rssiData.put(jsonRssiNetwork);

                } catch (
                        JSONException e) {
                    e.printStackTrace();
                }
            }

            //post the rssi data to the imaginary api
            OkHttpClient.Builder b = new OkHttpClient.Builder();
            b.readTimeout(4, TimeUnit.MINUTES);
            b.writeTimeout(4, TimeUnit.MINUTES);
            OkHttpClient client = b.build();

            Retrofit retrofit3 = new Retrofit.Builder()
                    .baseUrl(Environment.serviceURL)
                    .client(client)
                    .build();

            PostRssi postRssi = retrofit3.create(PostRssi.class);


            RequestBody jsonRequestFile =
                    RequestBody.create(
                            MediaType.parse("application/json"),
                            rssiData.toString());

            MultipartBody.Part rssiJsonMultiPart = MultipartBody.Part.createFormData("json", "rssi_json", jsonRequestFile);
            final JSONArray rssiNetworks = rssiData;

            Call<ResponseBody> call = postRssi.postRssiData(rssiJsonMultiPart);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    //To prove that the HTTP is made, the RSSI results is written to a json file called rssi_data
                    // under the app files folder located under internal storage/Android/data/com.stuartjacobs.rssiscout/files
                    try {
                        Writer output = null;
                        File file = new File(getExternalFilesDir(null), "rssi_data.json");
                        if (!file.exists()) {
                            file.createNewFile();
                            output = new BufferedWriter(new FileWriter(file));
                            output.write(rssiNetworks.toString());
                            output.close();
                        } else {
                            file.delete();
                            file.createNewFile();
                            output = new BufferedWriter(new FileWriter(file));
                            output.write(rssiNetworks.toString());
                            output.close();
                        }

                    } catch (Exception e) {
                    }

                }
            });


        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler = new Handler();
        handler.post(runnableService);
        return START_STICKY;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        if (runnableService != null) {
            handler.removeCallbacks(runnableService);
        }
        stopSelf();
        super.onDestroy();
    }

}
